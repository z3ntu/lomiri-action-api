/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_ACTION_QML_ACTION
#define LOMIRI_ACTION_QML_ACTION

namespace lomiri {
namespace action {
namespace qml {
    class Action;
}
}
}

#include <QObject>
#include <QVariant>
#include <QScopedPointer>

#include <lomiri/action/Action>

class Q_DECL_EXPORT lomiri::action::qml::Action : public lomiri::action::Action
{
    Q_OBJECT
    Q_DISABLE_COPY(Action)

public:
        explicit Action(QObject *parent = 0);
        virtual ~Action();
};
#endif
