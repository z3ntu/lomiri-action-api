/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_ACTION_QML_ACTION_MANAGER
#define LOMIRI_ACTION_QML_ACTION_MANAGER

#include <QQmlListProperty>
#include <lomiri/action/ActionManager>
#include <lomiri/action/Action>
#include <lomiri/action/ActionContext>

namespace lomiri {
namespace action {
namespace qml {
    class ActionManager;
}
}
}

class lomiri::action::qml::ActionManager : public lomiri::action::ActionManager
{
    Q_OBJECT
    Q_DISABLE_COPY(ActionManager)

    Q_PROPERTY(QQmlListProperty<lomiri::action::ActionContext> localContexts
               READ localContexts_list)
    Q_PROPERTY(QQmlListProperty<lomiri::action::Action> actions
               READ actions_list)

    Q_CLASSINFO("DefaultProperty", "actions")

public:

    ActionManager(QObject *parent = 0);
    virtual ~ActionManager();

    QQmlListProperty<lomiri::action::ActionContext> localContexts_list();
    QQmlListProperty<lomiri::action::Action> actions_list();

private:
    static void contextAppend(QQmlListProperty<ActionContext> *list, ActionContext *context);
    static void contextClear(QQmlListProperty<ActionContext> *list);
    static int contextCount(QQmlListProperty<ActionContext> *list);

    static void actionAppend(QQmlListProperty<Action> *list, Action *action);
    static void actionClear(QQmlListProperty<Action> *list);
    static int actionCount(QQmlListProperty<Action> *list);
};
#endif









