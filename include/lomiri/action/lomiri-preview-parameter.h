/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_ACTION_PREVIEW_PARAMETER
#define LOMIRI_ACTION_PREVIEW_PARAMETER

namespace lomiri {
namespace action {
    class PreviewParameter;
}
}

#include <QObject>

class Q_DECL_EXPORT lomiri::action::PreviewParameter : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(PreviewParameter)

public:

    virtual ~PreviewParameter();

protected:
    explicit PreviewParameter(QObject *parent = 0);

private:
    class Private;
    QScopedPointer<Private> d;
};

#endif
