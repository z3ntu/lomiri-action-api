/* This file is part of lomiri-action-api
 * Copyright 2013 Canonical Ltd.
 *
 * lomiri-action-api is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * lomiri-action-api is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_ACTION_PREVIEW_ACTION
#define LOMIRI_ACTION_PREVIEW_ACTION

namespace lomiri {
namespace action {
    class PreviewAction;
    class PreviewParameter;
}
}

#include "lomiri-action.h"

class Q_DECL_EXPORT lomiri::action::PreviewAction : public lomiri::action::Action
{
    Q_OBJECT
    Q_DISABLE_COPY(PreviewAction)
    Q_PROPERTY(QString commitLabel
               READ commitLabel
               WRITE setCommitLabel
               NOTIFY commitLabelChanged)

public:

    explicit PreviewAction(QObject *parent = 0);
    virtual ~PreviewAction();

    QString commitLabel() const;
    void setCommitLabel(const QString &value);

    QList<PreviewParameter *> parameters();
    Q_INVOKABLE void addParameter(lomiri::action::PreviewParameter *parameter);
    Q_INVOKABLE void removeParameter(lomiri::action::PreviewParameter *parameter);

signals:
    void started();
    void cancelled();
    void resetted();

    void commitLabelChanged(const QString &value);

    void parametersChanged();

private:
    class Private;
    QScopedPointer<Private> d;
};

#endif
